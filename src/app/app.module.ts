import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { setTheme } from 'ngx-bootstrap/utils';
import { AppComponent } from './app.component';
import { ReportPrintModule } from './report-print/report.print.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReportPrintModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() { 
    setTheme('bs4');
  }
}
