import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'report-print',
  templateUrl: './report-print.component.html',
  styleUrls: ['./report-print.component.scss']
})
export class ReportPrintComponent implements OnInit {

  @Input() MODE: string;
  @Input() STANDARD: string;
  @Input() POP_TITLE: string;
  @Input() SHOW_BUTTON: boolean;
  @Input() BUTTON_TEXT: string;
  @Input() BUTTON_CLASS: Object;
  @Input() PRINT_HTML: any;
  @Input() PRINT_STYLE: string;
  @Input() PRINT_CSS: string[];
  @Output() PRINT_COMPLETE: EventEmitter<any>;

  private modes: any;
  private standards: any;
  private oldBtnText: string;
  private printWindow: Window;
  private printDoc: Document;

  constructor() {
    this.modes = {
      iframe: 'iframe',
      popup: 'popup'
    };
    this.standards = {
      strict: 'strict',
      loose: 'loose',
      html5: 'html5'
    };
    this.MODE = this.modes.iframe;
    this.STANDARD = this.standards.html5;
    this.POP_TITLE = '';
    this.SHOW_BUTTON = true;
    this.BUTTON_CLASS = {
      'print-btn': true,
      'print-btn-success': true
    };
    this.POP_TITLE = 'Test';
    this.BUTTON_TEXT = 'Button Text';
    this.oldBtnText = this.BUTTON_TEXT;
    this.PRINT_COMPLETE = new EventEmitter<any>(false);
  }

  ngOnInit() {
  }

  private write(): any {
    this.printDoc.open();
    this.printDoc.write(`${this.docType()}${this.getHead()}${this.getBody()}`);
    this.printDoc.close();
  }

  private docType(): string {
    if (this.MODE === this.modes.iframe) {
      return '';
    }
    if (this.STANDARD === this.standards.html5) {
      return '<!DOCTYPE html>';
    }
    let transitional: string = this.STANDARD === this.standards.loose ? 'Transitional' : '',
      dtd: string = this.STANDARD === this.standards.loose ? 'loose' : 'strict';
    return `<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 ${transitional}//EN" "http://www.w3.org/TR/html4/${dtd}.dtd">`;
  }

  private getHead(): string {
    let styles: string = '',
      links: string = '';

    if (this.PRINT_CSS) {
      this.PRINT_CSS.forEach((url) => {
        links += `<link href="${url}" rel="stylesheet">`;
      });
    }

    if (this.PRINT_STYLE) {
      styles = `<style>${this.PRINT_STYLE}</style>`;
    }
    return `<head><title>${this.POP_TITLE}</title>${styles}${links}</head>`;
  }

  private getBody() {
    let html: string = '';
    if (this.PRINT_HTML) {
      if (this.PRINT_HTML.outerHTML) {
        html = this.PRINT_HTML.outerHTML;
      } else {
        html = this.PRINT_HTML;
      }
    }
    return `<body><object id="print-window-block" style="display: none;" height="0" classid="clsid:8856F961-340A-11D0-A96B-00C04FD705A2"></object>${html}</body>`;
  }


  private startPrint() {
    const timeoutId = setTimeout(() => {
      this.printWindow.focus();
      if (!!window['ActiveXObject'] || 'ActiveXObject' in window) {
        try {
          this.printWindow['print-window-block'].ExecWB(7, 1);
        } catch (e) {
          console.error(e);
        }
      } else {
        this.printWindow.print();
      }
      if (this.MODE === this.modes.popup) {
        const timeoutId2 = setTimeout(() => {
          clearTimeout(timeoutId2);
          this.printWindow.close();
        }, 500);
      }
      clearTimeout(timeoutId);
      this.PRINT_COMPLETE.emit();
      this.BUTTON_TEXT = this.oldBtnText;
      this.setInputAndTextareaValue(true);
    }, 1000);
  }

  private createIframe() {
    let oldFrame: any = document.getElementsByClassName('print-container');
    if (oldFrame.length > 0) {
      oldFrame[0].parentNode.removeChild(oldFrame[0]);
    }
    try {
      let printIframe: any = document.createElement('iframe');
      document.body.appendChild(printIframe);
      printIframe.style.position = 'absolute';
      printIframe.style.border = '0';
      printIframe.style.width = '0';
      printIframe.style.height = '0';
      printIframe.style.left = '0';
      printIframe.style.top = '0';
      printIframe.style.zIndex = '-1';
      printIframe.className = 'print-container';
      this.printWindow = printIframe.contentWindow;
      this.printDoc = printIframe.contentDocument ? printIframe.contentDocument : (printIframe.contentWindow ? printIframe.contentWindow.document : printIframe.document);
    }
    catch (e) {
      throw e + '. iframes may not be supported in this browser.';
    }

    if (!this.printWindow) {
      throw 'Cannot find window.';
    }

    if (!this.printDoc) {
      throw 'Cannot find document.';
    }
  }


  private createPopup() {
    let windowAttr = `location=yes,statusbar=no,directories=no,menubar=no,titlebar=no,toolbar=no,dependent=no`;
    windowAttr += `,width=${window.screen.width},height=${window.screen.height};`;
    windowAttr += ',resizable=yes,personalbar=no,scrollbars=yes';
    let newWin = window.open('', '_blank', windowAttr);
    this.printWindow = newWin;
    this.printDoc = newWin.document;
  }

  private getPrintWindow() {
    if (this.MODE === this.modes.iframe) {
      this.createIframe();
    } else if (this.MODE === this.modes.popup) {
      this.createPopup();
    }
  }

  print(printHTML?: any) {
    this.PRINT_HTML = printHTML ? printHTML : this.PRINT_HTML;
    if (!this.PRINT_HTML.outerHTML) {
      const div: HTMLDivElement = document.createElement('div');
      div.innerHTML = this.PRINT_HTML;
      this.PRINT_HTML = div;
    }
    this.oldBtnText = this.BUTTON_TEXT;
    this.BUTTON_TEXT = '准备打印...';
    const timeoutId = setTimeout(() => {
      clearTimeout(timeoutId);
      this.setInputAndTextareaValue();
      this.getPrintWindow();
      this.write();
      this.startPrint();
    }, 1000);
  }


  setInputAndTextareaValue(isReset: boolean = false) {
		const inputs: any = this.PRINT_HTML.getElementsByTagName('input');
		const excludeTypes: string[] = ['hidden', 'button', 'reset', 'submit'];
		const textareas: any = this.PRINT_HTML.getElementsByTagName('textarea');
		const selects: any = this.PRINT_HTML.getElementsByTagName('select');
		this.toArray(inputs).forEach((input: HTMLInputElement) => {
			if (excludeTypes.indexOf(input.type) < 0) {
				if (!isReset) {
					if (input.type === 'radio' || input.type === 'checkbox') {
						if (!input.getAttribute('checked') && input.checked) {
							input.setAttribute('isSetValue', 'true');
							input.setAttribute('checked', input.checked + '');
						}
					} else {
						if (!input.getAttribute('value')) {
							input.setAttribute('isSetValue', 'true');
							input.setAttribute('value', input.value);
						}
					}
				} else {
					if (input.type === 'radio' || input.type === 'checkbox') {
						if (!!input.getAttribute('isSetValue')) {
							input.removeAttribute('checked');
							input.removeAttribute('isSetValue');
						}
					} else {
						if (!!input.getAttribute('isSetValue')) {
							input.removeAttribute('value');
							input.removeAttribute('isSetValue');
						}
					}
				}
			}
		});
		this.toArray(textareas).forEach((textarea: HTMLTextAreaElement) => {
			if (!isReset) {
				if (!textarea.innerHTML) {
					textarea.setAttribute('isSetHtml', 'true');
					textarea.innerHTML = textarea.value;
				}
			} else {
				if (!!textarea.getAttribute('isSetHtml')) {
					textarea.innerHTML = '';
					textarea.removeAttribute('isSetHtml');
				}
			}
		});
		this.toArray(selects).forEach((select: HTMLSelectElement) => {
			this.toArray(select.selectedOptions).forEach((option: HTMLOptionElement) => {
				if (!isReset) {
					if (!option.getAttribute('selected')) {
						option.setAttribute('isSetValue', 'true');
						option.setAttribute('selected', 'true');
					}
				} else {
					if (!!option.getAttribute('isSetValue')) {
						option.removeAttribute('selected');
						option.removeAttribute('isSetValue');
					}
				}
			});
		});
  }
  
  toArray(arr: any): any[] {
		return arr ? Array.prototype.slice.call(arr, 0) : [];
	}

}
