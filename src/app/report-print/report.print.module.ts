import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportPrintComponent } from './report-print.component';
import { ButtonsModule } from 'ngx-bootstrap';

@NgModule({
    declarations: [
        ReportPrintComponent
    ],
    imports: [
        CommonModule,
        ButtonsModule
    ],
    exports: [
        ReportPrintComponent
    ],
    providers: [],
})
export class ReportPrintModule {

}